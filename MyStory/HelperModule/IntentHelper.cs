﻿using System;
using Android.Content;
using StoryModels;

namespace HelperModule
{
	public static class IntentHelper
	{

		public static MediaEntry ManageMediaIntent(Context context, EntryType type, Intent data)
		{
			MediaEntry m = new MediaEntry ();
			m.TimeStamp = DateTime.Now;
			m.Type = type;

			if (data.Data != null)
			{
				m.MediaPaths.Add (UriHelper.GetPath (context, data.Data));
			} 
			else if (data.ClipData != null)
			{
				var items = data.ClipData;

				for (int i = 0; i < items.ItemCount; i++) {
					var item = (ClipData.Item)items.GetItemAt (i);
					m.MediaPaths.Add (UriHelper.GetPath (context, item.Uri));
				}
			}

			return m;
		}
			
	}
}

