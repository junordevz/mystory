﻿using System;
using Android.Content;
using Uri = Android.Net.Uri;
using Android.OS;
using Android.Provider;
using Android;
using Android.Database;

namespace HelperModule
{
	public static class UriHelper 
	{
		public static String GetPath(Context context, Uri uri) 
		{
			//check here to KITKAT or new version
			bool isKitKat = Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat;

			// DocumentProvider
			if (isKitKat && DocumentsContract.IsDocumentUri(context, uri)) {

				// ExternalStorageProvider
				if (isExternalStorageDocument(uri)) {
					string docId = DocumentsContract.GetDocumentId(uri);
					string[] split = docId.Split(':');
					string type = split[0];

					if (type.ToLower() == "primary") {
						return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
					}
				}
				// DownloadsProvider
				else if (isDownloadsDocument(uri)) {

					string id = DocumentsContract.GetDocumentId(uri);
					Uri contentUri = ContentUris.WithAppendedId(
						Uri.Parse("content://downloads/public_downloads"), long.Parse(id));

					return getDataColumn(context, contentUri, null, null);
				}
				// MediaProvider
				else if (isMediaDocument(uri)) {
					string docId = DocumentsContract.GetDocumentId(uri);
					string[] split = docId.Split(':');
					string type = split[0];

					Uri contentUri = null;
					if (type == "image") {
						contentUri = MediaStore.Images.Media.ExternalContentUri;
					} else if (type == "video") {
						contentUri = MediaStore.Video.Media.ExternalContentUri;
					} else if (type == "audio") {
						contentUri = MediaStore.Audio.Media.ExternalContentUri;
					}

					string selection = "_id=?";
					string[] selectionArgs = new String[] {
						split[1]
					};

					return getDataColumn(context, contentUri, selection, selectionArgs);
				}
			}
			// MediaStore (and general)
			else if (uri.Scheme.ToLower() == "content") {

				// Return the remote address
				if (isGooglePhotosUri(uri))
					return uri.LastPathSegment;

				return getDataColumn(context, uri, null, null);
			}
			// File
			else if (uri.Scheme.ToLower() == "file") {
				return uri.Path;
			}

			return null;
		}
			
		private static String getDataColumn(Context context, Uri uri, string selection, string[] selectionArgs)
		{
			ICursor cursor = null;
			string column = "_data";
			string[] projection = {
				column
			};

			try {
				cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs, null);
				if (cursor != null && cursor.MoveToFirst()) {
					int index = cursor.GetColumnIndexOrThrow(column);
					return cursor.GetString(index);
				}
			} finally {
				if (cursor != null)
					cursor.Close();
			}
			return null;
		}
			
		private static bool isExternalStorageDocument(Uri uri) {
			return uri.Authority == "com.android.externalstorage.documents";
		}

		private static bool isDownloadsDocument(Uri uri) {
			return uri.Authority == "com.android.providers.downloads.documents";
		}

		private static bool isMediaDocument(Uri uri) {
			return uri.Authority == "com.android.providers.media.documents";
		}

		private static bool isGooglePhotosUri(Uri uri) {
			return uri.Authority == "com.google.android.apps.photos.content";
		}
	}
}


