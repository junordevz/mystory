﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Provider;
using System.IO;
using Android.Database;
using Newtonsoft.Json;
using Android.Webkit;
using System.Collections.Generic;
using HelperModule;
using StoryModels;
using StorageModule;

namespace MyStory
{
	[Activity (Label = "MyStory", Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		int count = 1;
		public static readonly int PickImageId = 1000;
		public static readonly int PickVideoId = 1001;

		private ImageView img;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			img = FindViewById<ImageView> (Resource.Id.imageView1);
			img.SetAdjustViewBounds (true);
			img.SetScaleType(ImageView.ScaleType.FitCenter);

			button.Click += delegate {
				Intent = new Intent();
				Intent.SetType("image/*");
				//Intent.SetType("video/*");
				//Intent.SetType("audio/*");
				Intent.SetAction(Intent.ActionGetContent);
				Intent.PutExtra(Intent.ExtraAllowMultiple, true);
				StartActivityForResult(Intent.CreateChooser(Intent, "Select Picture"), PickImageId);//PickVideoId);

				button.Text = string.Format ("{0} clicks!", count++);
			};

			Story s1 = new Story ();
			s1.Name = "Story1";
			s1.Entries.Add (new TextEntry { Title = "Entry1", Content = "Content1" });

			StorageManager.StoreStory (s1);

			Story s2 = new Story ();
			s2.Name = "Story2";
			s2.Entries.Add (new TextEntry { Title = "Entry2", Content = "Content2" });
			s2.Entries.Add (new MapEntry { Title = "Entry3", Content = "Content3", Position = new LatLon { Lat = 45, Lon = 21 }} );
		
			StorageManager.StoreStory (s2);

			var ls = StorageManager.GetStories ();
			bool ex = StorageManager.StoryExists (s1.Name);

			s2 = null;
			s2 = StorageManager.GetStory (ls [1]);

			s1 = null;
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			if ((requestCode == PickImageId) && (resultCode == Result.Ok) && (data != null))
			{
				 //var dt = IntentHelper.ManageMediaIntent (this, StoryModels.MediaType.Image, data);
				//dt = null;
				/*
				Bitmap i = MediaStore.Images.Media.GetBitmap(this.ContentResolver, uri);
				double ratio = (double)i.Width / i.Height;
				int newH = 500;
				int newW = Convert.ToInt32(ratio * newH);
				Bitmap scaled = Bitmap.CreateScaledBitmap(i, newW, newH, true);
				i.Recycle();

				img.SetImageBitmap (scaled);
				*/
			}
			if ((requestCode == PickVideoId) && (resultCode == Result.Ok) && (data != null))
			{
				Android.Net.Uri uri = data.Data;

				string path = UriManager.getPath (this, uri);
				Android.Net.Uri pathUri = Android.Net.Uri.FromFile (new Java.IO.File (path));

				Intent intent = new Intent(Intent.ActionView);
				intent.SetDataAndType (pathUri, "video/*");
				StartActivity(intent);
			}
		}
	}
}


