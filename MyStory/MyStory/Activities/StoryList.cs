﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyStory.ArrayAdapters;
using StoryModels;
using StorageModule;
using System.Threading;

namespace MyStory
{
	[Activity (Label = "MyStory", MainLauncher = true, Icon = "@drawable/icon")]		
	public class StoryList : Activity
	{
		private bool doReset = false;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Window.RequestFeature(WindowFeatures.NoTitle);
			SetContentView (Resource.Layout.StoryListActivity);

			//ExampleJsons ();

			List<StoryEntry> entries = StorageManager.GetStoriesList ();
			StoriesAdapter adapter = new StoriesAdapter (this, entries);

			ListView storiesLv = FindViewById<ListView> (Resource.Id.storiesList);
			storiesLv.Adapter = adapter;

			storiesLv.ItemClick += (sender, e) => {
				AppContext.StoryName = entries[(int)e.Id].Name;
				doReset = true;
				StartActivity (typeof(StoryTimeline));
			};

			SearchView storySh = FindViewById<SearchView> (Resource.Id.storySearch);
			storySh.QueryTextChange += (sender, e) => adapter.Filter.InvokeFilter(e.NewText);

			ImageView addImg = FindViewById<ImageView> (Resource.Id.addImg);
			addImg.Click += (sender, e) => AddNewTrip ();
		}

		private void AddNewTrip()
		{
			Context context = this;
			EditText input = new EditText(this);

			new AlertDialog.Builder (this)
				.SetPositiveButton ("Let`s Go!", (sender, args) => {
					string val = input.Text;
					if (StorageManager.StoryExists(val))
						RunOnUiThread(() => Toast.MakeText(this, "Story already told. (Exists)", ToastLength.Long).Show());
					else
						RunOnUiThread(() => {
							Story s = new Story();
							s.Name = val;
							StorageManager.StoreStory(s);

							AppContext.StoryName = val;
							doReset = true;
							StartActivity(typeof(StoryTimeline));
						});
				})
				.SetNegativeButton ("Not now...", (sender, args) => {

				})
				.SetMessage ("Give a Title to your new Story!")
				.SetTitle ("Once upon a time...")
				.SetView (input)
				.Show();
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			if (doReset) {
				doReset = false;
				Recreate ();
			}
		}

		private void ExampleJsons()
		{
			Story s1 = new Story ();
			s1.Name = "Story Timisoara";
			s1.Entries.Add (new TextEntry () { Id = 0 }); Thread.Sleep(10);
			s1.Entries.Add (new TextEntry () { Id = 1 }); Thread.Sleep(10);

			Story s2 = new Story ();
			s2.Name = "Story Bucuresti";

			Story s3 = new Story ();
			s3.Name = "Story Craiova";
			s3.Entries.Add (new TextEntry () { Id = 0, Title = "Title" }); Thread.Sleep(10);
			s3.Entries.Add (new MapEntry () { Id = 1, Title = "Title" }); Thread.Sleep(10);
			s3.Entries.Add (new MediaEntry (EntryType.Image) { Id = 2, Title = "Title" }); Thread.Sleep(10);
			s3.Entries.Add (new MediaEntry (EntryType.Video) { Id = 3, Title = "Title" }); Thread.Sleep(10);
			s3.Entries.Add (new MediaEntry (EntryType.Audio) { Id = 4, Title = "Title" }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 5 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 6 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 7 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 8 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 9 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 10 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 11 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 12 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 13 }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 14, Title = "Title1" }); Thread.Sleep(10);
			s3.Entries.Add (new MapEntry () { Id = 15, Title = "Title2" }); Thread.Sleep(10);
			s3.Entries.Add (new TextEntry () { Id = 16, Title = "Title3" }); Thread.Sleep(10);
			s3.Entries.Add (new MapEntry () { Id = 17, Title = "Title4" }); Thread.Sleep(10);

			StorageManager.StoreStory (s1);
			StorageManager.StoreStory (s2);
			StorageManager.StoreStory (s3);
		}
	}
}

