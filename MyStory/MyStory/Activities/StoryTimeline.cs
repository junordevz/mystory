﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MyStory.ArrayAdapters;
using StorageModule;
using StoryModels;

namespace MyStory
{
	[Activity (Label = "StoryTimeline")]			
	public class StoryTimeline : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Window.RequestFeature(WindowFeatures.NoTitle);
			SetContentView (Resource.Layout.StoryTimelineActivity);

			TextView nameT = FindViewById<TextView> (Resource.Id.nameText);
			nameT.Text = AppContext.StoryName;

			List<IEntry> entries = StorageManager.GetStory (AppContext.StoryName).Entries;

			EntriesAdapter adapter = new EntriesAdapter (this, entries);
			ListView entriesLv = FindViewById<ListView> (Resource.Id.entriesList);
			entriesLv.Adapter = adapter;
			entriesLv.ItemClick += (sender, e) => {
				AppContext.Entry = entries.First(x => x.GetId() == e.Id);

				StartActivity(typeof(EntryDetails));
			};

			ImageView addImg = FindViewById<ImageView> (Resource.Id.addImg);
			addImg.Click += (sender, e) => {
				StartActivity(typeof(AddEntry));
			};

			ImageView delI = FindViewById<ImageView> (Resource.Id.remImg);
			delI.Click += (sender, e) => {
				new AlertDialog.Builder (this)
					.SetPositiveButton ("I said, THE END!", (sender2, args) => {
						RunOnUiThread(() => {
							if (StorageManager.RemoveStory(AppContext.StoryName)) {
								AppContext.RefreshFlag = true;
								Finish();
							}
							else
								Toast.MakeText(this, "Delete Failed!", ToastLength.Short).Show();
						});
					})
					.SetNegativeButton ("No!", (sender2, args) => {

					})
					.SetMessage ("Delete entire Story?!")
					.SetTitle ("...The End!")
					.Show();
			};
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			if (AppContext.RefreshFlag == true) {
				AppContext.RefreshFlag = false;
				Recreate ();
			}
		}
	}
}

