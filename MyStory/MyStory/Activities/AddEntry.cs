﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using StoryModels;
using StorageModule;
using HelperModule;

namespace MyStory
{
	[Activity (Label = "Add new Memory")]			
	public class AddEntry : Activity
	{
		public static readonly int PickImageId = 1000;
		public static readonly int PickVideoId = 1001;

		private DateTime date;
		private TimeSpan time;

		private Button dateB, timeB;
		private EditText titleT, contT;
		private RadioButton imageR, videoR;

		private MediaEntry toAdd;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Window.RequestFeature(WindowFeatures.NoTitle);
			SetContentView (Resource.Layout.AddEntryActivity);

			titleT = FindViewById<EditText> (Resource.Id.titleEdit);
			contT = FindViewById<EditText> (Resource.Id.contentEdit);

			imageR = FindViewById<RadioButton> (Resource.Id.imgR);
			videoR = FindViewById<RadioButton> (Resource.Id.videoR);

			Button doneB = FindViewById<Button> (Resource.Id.doneB);
			doneB.Click += (sender, e) => {
				DateTime stamp = date.Add(time);

				if (imageR.Checked == false && videoR.Checked == false)
				{
					TextEntry t = new TextEntry();
					t.Title = titleT.Text;
					t.Content = contT.Text;
					t.TimeStamp = stamp;
					t.Type = EntryType.Text;

					StorageManager.AddEntryToStory(AppContext.StoryName, t);
					AppContext.RefreshFlag = true;
					RunOnUiThread(() => Finish());
				}
				else 
				{
					toAdd = new MediaEntry();
					toAdd.Title = titleT.Text;
					toAdd.Content = contT.Text;
					toAdd.TimeStamp = stamp;

					string type = "", msg = "";
					if (imageR.Checked)
					{
						toAdd.Type = EntryType.Image;
						type = "image/*";
						msg = "Select a Picture";
					}
					else
					{
						toAdd.Type = EntryType.Video;
						type = "video/*";
						msg = "Select a Video";
					}

					Intent = new Intent();
					Intent.SetType(type);
					Intent.SetAction(Intent.ActionGetContent);
					//Intent.PutExtra(Intent.ExtraAllowMultiple, true);
					StartActivityForResult(Intent.CreateChooser(Intent, msg), PickImageId);
				}
			};

			SetUpDateTime ();
		}

		private void SetUpDateTime()
		{
			dateB = FindViewById<Button> (Resource.Id.dateBtn);
			dateB.Click += (sender, e) => {
				(new DatePickerDialog (this, OnDateSet, date.Year, date.Month - 1, date.Day)).Show ();
			};

			timeB = FindViewById<Button> (Resource.Id.timeBtn);
			timeB.Click += (sender, e) => {
				(new TimePickerDialog (this, OnTimeSet, time.Hours, time.Minutes, true)).Show ();
			};

			date = DateTime.Today;
			time = DateTime.Now.TimeOfDay;
			UpdateDateUI ();
		}

		private void UpdateDateUI()
		{
			dateB.Text = date.ToString (@"dd/MM/yyyy");
			timeB.Text = time.ToString (@"hh\:mm");
		}

		private void OnDateSet (object sender, DatePickerDialog.DateSetEventArgs e)
		{
			DateTime newD = e.Date;
			if (date != newD) {
				date = newD;
				UpdateDateUI ();
			}
		}

		private void OnTimeSet (object sender, TimePickerDialog.TimeSetEventArgs e)
		{
			TimeSpan newT = new TimeSpan(e.HourOfDay, e.Minute, 0);
			if (time != newT) {
				time = newT;
				UpdateDateUI ();
			}
		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			if ((resultCode == Result.Ok) && (data != null)) {
				Android.Net.Uri uri = data.Data;

				string path = UriManager.getPath (this, uri);

				toAdd.MediaPaths.Add (path);

				StorageManager.AddEntryToStory (AppContext.StoryName, toAdd);
				AppContext.RefreshFlag = true;
				RunOnUiThread (() => Finish ());
			} else
				Toast.MakeText (this, "Canceled.", ToastLength.Long);
		}

	}
}

