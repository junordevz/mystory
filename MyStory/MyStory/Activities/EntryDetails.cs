﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using StorageModule;
using StoryModels;

namespace MyStory
{
	[Activity (Label = "EntryDetails")]			
	public class EntryDetails : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			Window.RequestFeature(WindowFeatures.NoTitle);
			SetContentView (Resource.Layout.ViewEntryActivity);
		
			TextView nameT = FindViewById<TextView> (Resource.Id.nameText);
			nameT.Text = AppContext.Entry.GetTitle ();

			TextView timeT = FindViewById<TextView> (Resource.Id.timestampText);
			timeT.Text = AppContext.Entry.GetTimeStamp ().ToString (@"dd/MM/yyyy HH:mm");

			TextView detailsT = FindViewById<TextView> (Resource.Id.descriptionText);
			detailsT.Text = AppContext.Entry.GetContent ();

			ImageView delI = FindViewById<ImageView> (Resource.Id.remImg);
			delI.Click += (sender, e) => {
				new AlertDialog.Builder (this)
					.SetPositiveButton ("Yes, Forget this...", (sender2, args) => {
						RunOnUiThread(() => {
							if (StorageManager.RemoveEntryFromStory(AppContext.StoryName, AppContext.Entry.GetId())) {
								AppContext.RefreshFlag = true;
								Finish();
							}
							else
								Toast.MakeText(this, "Delete Failed!", ToastLength.Short).Show();
						});
					})
					.SetNegativeButton ("No!", (sender2, args) => {

					})
					.SetMessage ("Delete entry form Story?")
					.SetTitle ("Forget Entry")
					.Show();
			};

			Button showB = FindViewById<Button> (Resource.Id.openBtn);
			showB.Click += (sender, e) => {
				if (AppContext.Entry.GetType() == StoryModels.EntryType.Text)
					RunOnUiThread(() => Toast.MakeText(this, "Text already displayed.", ToastLength.Long).Show());
				else
				{
					Android.Net.Uri pathUri = Android.Net.Uri.FromFile (new Java.IO.File (((MediaEntry)AppContext.Entry).MediaPaths[0]));

					string type = "image/*";
					if (AppContext.Entry.GetType() == StoryModels.EntryType.Video)
						type = "video/*";

					Intent intent = new Intent(Intent.ActionView);
					intent.SetDataAndType (pathUri, type);
					StartActivity(intent);
				}


			};
		}
	}
}

