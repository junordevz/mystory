﻿using System;
using Android.Widget;
using StoryModels;
using System.Collections.Generic;
using Android.Views;
using Android.App;
using System.Linq;

namespace MyStory.ArrayAdapters
{
	public class EntriesAdapter : BaseAdapter<IEntry>, IFilterable
	{
		Activity activity;
		List<IEntry> data;
		List<IEntry> original;
		EntriesFilter filter;

		public EntriesAdapter(Activity activity, List<IEntry> data)
		{
			this.activity = activity;
			this.data = data;
			this.original = data;
			this.filter = new EntriesFilter (this);
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return new Java.Lang.String (data [position].GetTimeStamp().ToString());
		}

		public override long GetItemId (int position)
		{
			return data[position].GetId ();
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? activity.LayoutInflater.Inflate (Resource.Layout.EntryItem, parent, false);

			DateTime dt = data [position].GetTimeStamp ();

			var dateT = view.FindViewById<TextView> (Resource.Id.dateText);
			dateT.Text = dt.ToString (@"dd/MM/yyyy");

			var timeT = view.FindViewById<TextView> (Resource.Id.timeText);
			timeT.Text = dt.ToString (@"hh\:mm");

			var titleT = view.FindViewById<TextView> (Resource.Id.titleText);
			titleT.Text = data[position].GetTitle ();

			var typeI = view.FindViewById<ImageView> (Resource.Id.typeImg);

			switch (data [position].GetType ()) {
			case EntryType.Text:
				typeI.SetImageResource (Resource.Drawable.text);
				break;
			case EntryType.Map:
				typeI.SetImageResource (Resource.Drawable.map);
				break;
			case EntryType.Image:
				typeI.SetImageResource (Resource.Drawable.image);
				break;
			case EntryType.Video:
				typeI.SetImageResource (Resource.Drawable.video);
				break;
			case EntryType.Audio:
				typeI.SetImageResource (Resource.Drawable.audio);
				break;
			}

			return view;
		}
			

		public override int Count { get { return data.Count; } }

		public Filter Filter { get { return filter; } }

		public override IEntry this [int index] { get { return data [index]; } }

		private class EntriesFilter : Filter 
		{
			EntriesAdapter customAdapter;
			public EntriesFilter (EntriesAdapter adapter) : base() {
				customAdapter = adapter;
			}

			protected override FilterResults PerformFiltering (Java.Lang.ICharSequence constraint)
			{
				FilterResults results = new FilterResults();
				if (constraint != null) {
					string searchFor = constraint.ToString ().ToLower ();
					List<IEntry> matchList = new List<IEntry>();
					matchList.AddRange (customAdapter.original.Where (x => x.GetTitle().ToLower ().IndexOf(searchFor) >= 0));

					results.Values = FromArray (matchList.Select (x => x.ToJavaObject ()).ToArray());
					results.Count = matchList.Count;

					constraint.Dispose();
				}
				return results;
			}

			protected override void PublishResults (Java.Lang.ICharSequence constraint, FilterResults results)
			{
				using (var values = results.Values)
					if (values != null)
						customAdapter.data = values.ToArray<Java.Lang.Object> ().Select (x => x.ToNetObject<IEntry> ()).ToList ();

				if (constraint != null) constraint.Dispose();
				if (results != null) results.Dispose();

				customAdapter.NotifyDataSetChanged();
			}
		}
	}
}

