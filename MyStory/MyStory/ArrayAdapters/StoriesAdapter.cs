﻿using System;
using Android.Widget;
using StoryModels;
using System.Collections.Generic;
using Android.Views;
using Android.App;
using System.Linq;

namespace MyStory.ArrayAdapters
{
	public class StoriesAdapter : BaseAdapter<StoryEntry>, IFilterable
	{
		Activity activity;
		List<StoryEntry> data;
		List<StoryEntry> original;
		StoryFilter filter;

		public StoriesAdapter(Activity activity, List<StoryEntry> data)
		{
			this.activity = activity;
			this.data = data;
			this.original = data;
			this.filter = new StoryFilter (this);
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return new Java.Lang.String (data [position].Name);
		}

		public override long GetItemId (int position)
		{
			return data[position].Id;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			var view = convertView ?? activity.LayoutInflater.Inflate (Resource.Layout.StoryItem, parent, false);

			var nameT = view.FindViewById<TextView> (Resource.Id.nameText);
			nameT.Text = data [position].Name;

			var countT = view.FindViewById<TextView> (Resource.Id.countText);
			countT.Text = data [position].ElemenCount.ToString ();

			return view;
		}
			

		public override int Count { get { return data.Count; } }

		public Filter Filter { get { return filter; } }

		public override StoryEntry this [int index] { get { return data [index]; } }

		private class StoryFilter : Filter 
		{
			StoriesAdapter customAdapter;
			public StoryFilter (StoriesAdapter adapter) : base() {
				customAdapter = adapter;
			}

			protected override FilterResults PerformFiltering (Java.Lang.ICharSequence constraint)
			{
				FilterResults results = new FilterResults();
				if (constraint != null) {
					string searchFor = constraint.ToString ().ToLower ();
					List<StoryEntry> matchList = new List<StoryEntry>();
					matchList.AddRange (customAdapter.original.Where (x => x.Name.ToLower ().IndexOf(searchFor) >= 0));

					results.Values = FromArray (matchList.Select (x => x.ToJavaObject ()).ToArray());
					results.Count = matchList.Count;

					constraint.Dispose();
				}
				return results;
			}

			protected override void PublishResults (Java.Lang.ICharSequence constraint, FilterResults results)
			{
				using (var values = results.Values)
					if (values != null)
						customAdapter.data = values.ToArray<Java.Lang.Object> ().Select (x => x.ToNetObject<StoryEntry> ()).ToList ();

				if (constraint != null) constraint.Dispose();
				if (results != null) results.Dispose();

				customAdapter.NotifyDataSetChanged();
			}
		}
	}
}

