﻿using System;
using StoryModels;

namespace MyStory
{
	public static class AppContext
	{
		public static string StoryName { get; set; }

		public static IEntry Entry { get; set; }

		public static bool RefreshFlag { get; set; }
	}
}

