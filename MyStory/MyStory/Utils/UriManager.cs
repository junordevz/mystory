﻿using System;
using Android.Content;
using Uri = Android.Net.Uri;
using Android.OS;
using Android.Provider;
using Android;
using Android.Database;

namespace MyStory
{
	public static class UriManager 
	{
		/**
		  * Method for return file path of Gallery image 
		  * 
		  * @param context
		  * @param uri
		  * @return path of the selected image file from gallery
		  */
		public static String getPath(Context context, Uri uri) 
		{

			//check here to KITKAT or new version
			bool isKitKat = Build.VERSION.SdkInt >= BuildVersionCodes.Kitkat;

			// DocumentProvider
			if (isKitKat && DocumentsContract.IsDocumentUri(context, uri)) {

				// ExternalStorageProvider
				if (isExternalStorageDocument(uri)) {
					string docId = DocumentsContract.GetDocumentId(uri);
					string[] split = docId.Split(':');
					string type = split[0];

					if (type.ToLower() == "primary") {
						return Android.OS.Environment.ExternalStorageDirectory + "/" + split[1];
					}
				}
				// DownloadsProvider
				else if (isDownloadsDocument(uri)) {

					string id = DocumentsContract.GetDocumentId(uri);
					Uri contentUri = ContentUris.WithAppendedId(
						Uri.Parse("content://downloads/public_downloads"), long.Parse(id));

					return getDataColumn(context, contentUri, null, null);
				}
				// MediaProvider
				else if (isMediaDocument(uri)) {
					string docId = DocumentsContract.GetDocumentId(uri);
					string[] split = docId.Split(':');
					string type = split[0];

					Uri contentUri = null;
					if (type == "image") {
						contentUri = MediaStore.Images.Media.ExternalContentUri;
					} else if (type == "video") {
						contentUri = MediaStore.Video.Media.ExternalContentUri;
					} else if (type == "audio") {
						contentUri = MediaStore.Audio.Media.ExternalContentUri;
					}

					string selection = "_id=?";
					string[] selectionArgs = new String[] {
						split[1]
					};

					return getDataColumn(context, contentUri, selection, selectionArgs);
				}
			}
			// MediaStore (and general)
			else if (uri.Scheme.ToLower() == "content") {

				// Return the remote address
				if (isGooglePhotosUri(uri))
					return uri.LastPathSegment;

				return getDataColumn(context, uri, null, null);
			}
			// File
			else if (uri.Scheme.ToLower() == "file") {
				return uri.Path;
			}

			return null;
		}

		/**
		  * Get the value of the data column for this Uri. This is useful for
		  * MediaStore Uris, and other file-based ContentProviders.
		  *
		  * @param context The context.
		  * @param uri The Uri to query.
		  * @param selection (Optional) Filter used in the query.
		  * @param selectionArgs (Optional) Selection arguments used in the query.
		  * @return The value of the _data column, which is typically a file path.
		  */
		public static String getDataColumn(Context context, Uri uri, string selection, string[] selectionArgs)
		{
			ICursor cursor = null;
			string column = "_data";
			string[] projection = {
				column
			};

			try {
				cursor = context.ContentResolver.Query(uri, projection, selection, selectionArgs, null);
				if (cursor != null && cursor.MoveToFirst()) {
					int index = cursor.GetColumnIndexOrThrow(column);
					return cursor.GetString(index);
				}
			} finally {
				if (cursor != null)
					cursor.Close();
			}
			return null;
		}

		/**
		  * @param uri The Uri to check.
		  * @return Whether the Uri authority is ExternalStorageProvider.
		  */
		public static bool isExternalStorageDocument(Uri uri) {
			return uri.Authority == "com.android.externalstorage.documents";
		}

		/**
		  * @param uri The Uri to check.
		  * @return Whether the Uri authority is DownloadsProvider.
		  */
		public static bool isDownloadsDocument(Uri uri) {
			return uri.Authority == "com.android.providers.downloads.documents";
		}

		/**
		  * @param uri The Uri to check.
		  * @return Whether the Uri authority is MediaProvider.
		  */
		public static bool isMediaDocument(Uri uri) {
			return uri.Authority == "com.android.providers.media.documents";
		}

		/**
		  * @param uri The Uri to check.
		  * @return Whether the Uri authority is Google Photos.
		  */
		public static bool isGooglePhotosUri(Uri uri) {
			return uri.Authority == "com.google.android.apps.photos.content";
		}
	}
}

