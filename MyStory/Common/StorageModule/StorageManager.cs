﻿using System;
using System.IO;
using System.Linq;
using StoryModels;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace StorageModule
{
	public static class StorageManager
	{
		public static string GetStorageFolder()
		{
			return Environment.GetFolderPath (Environment.SpecialFolder.Personal);
		}

		public static string GetStoryPath(string StoryName)
		{
			string folder = GetStorageFolder ();
			return Path.Combine (folder, StoryName + ".json");
		}

		public static List<string> GetStories()
		{
			return Directory.EnumerateFiles (GetStorageFolder(), "*.json").Select(x => Path.GetFileNameWithoutExtension(x)).ToList();
		}

		public static List<StoryEntry> GetStoriesList()
		{
			List<StoryEntry> rez = new List<StoryEntry> ();
			long id = 0;
			foreach (string name in GetStories()) {
				Story s = GetStory (name);
				rez.Add (new StoryEntry {
					Id = id,
					Name = s.Name,
					ElemenCount = s.Entries.Count
				});
				id++;
			}
			return rez;
		}

		public static bool StoryExists (string Name)
		{
			return File.Exists (GetStoryPath (Name));
		}

		public static bool AddEntryToStory(string name, IEntry entry)
		{
			Story s = GetStory (name);
			if (s == null)
				return false;

			s.Entries.Add (entry);
			s.Entries = s.Entries.OrderBy (x => x.GetTimeStamp()).ToList();
			long id = 0;
			foreach (var item in s.Entries) {
				item.SetId (id);
				id += 1;
			}

			return StoreStory (s);
		}

		public static bool RemoveEntryFromStory(string name, long entryId)
		{
			Story s = GetStory (name);
			if (s == null)
				return false;
				
			s.Entries = s.Entries.OrderBy (x => x.GetTimeStamp()).ToList();
			s.Entries.Remove (s.Entries.First (x => x.GetId() == entryId));
			long id = 0;
			foreach (var item in s.Entries) {
				item.SetId (id);
				id += 1;
			}

			return StoreStory (s);
		}

		public static bool StoreStory (Story story, bool overrideFile = true)
		{
			if (StoryExists (story.Name) && overrideFile == false)
				return false;

			File.WriteAllText (GetStoryPath (story.Name), JsonConvert.SerializeObject (story));
			return true;
		}

		public static Story GetStory (string Name)
		{
			if (StoryExists (Name) == false)
				return null;
			else
				return JsonConvert.DeserializeObject<Story> (File.ReadAllText (GetStoryPath (Name)), new EntryConverter());
		}

		public static bool RemoveStory (string Name)
		{
			if (StoryExists (Name) == false)
				return false;

			File.Delete (GetStoryPath (Name));
			return true;
		}

	}
}

