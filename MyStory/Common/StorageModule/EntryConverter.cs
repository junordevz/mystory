﻿using System;
using StoryModels;
using Newtonsoft.Json.Linq;

namespace StorageModule
{
	public class EntryConverter : JsonCreationConverter<IEntry>
	{
		protected override IEntry Create(Type objectType, JObject jObject)
		{
			if (FieldExists("Position", jObject))
			{
				return new MapEntry();
			}
			else if (FieldExists("MediaPaths", jObject))
			{
				return new MediaEntry();
			}
			else
			{
				return new TextEntry();
			}
		}

		private bool FieldExists(string fieldName, JObject jObject)
		{
			return jObject[fieldName] != null;
		}
	}
}

