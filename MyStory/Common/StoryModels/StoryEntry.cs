﻿using System;

namespace StoryModels
{
	public class StoryEntry
	{
		public long Id { get; set; }

		public string Name { get; set; }

		public int ElemenCount { get; set; }

		public StoryEntry ()
		{
			Id = 0;
			Name = "";
			ElemenCount = 0;
		}
	}
}

