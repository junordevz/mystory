﻿using System;
using System.Collections.Generic;

namespace StoryModels
{
	[Serializable]
	public class MediaEntry : TextEntry, IEntry
	{
		public List<string> MediaPaths { get; set; }

		public MediaEntry () : base ()
		{
			Type = EntryType.Image;
			MediaPaths = new List<string>();
		}

		public MediaEntry (EntryType type) : base ()
		{
			Type = type;
			MediaPaths = new List<string>();
		}
	}
}

