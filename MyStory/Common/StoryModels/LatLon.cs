﻿using System;

namespace StoryModels
{
	[Serializable]
	public class LatLon
	{
		public double Lat { get; set; }

		public double Lon { get; set; }

		public LatLon ()
		{
			Lat = 0;
			Lon = 0;
		}
	}
}

