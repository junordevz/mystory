﻿using System;
using System.Collections.Generic;

namespace StoryModels
{
	[Serializable]
	public class Story
	{
		public string Name { get; set; }

		public List<IEntry> Entries { get; set;}

		public Story ()
		{
			Name = "";
			Entries = new List<IEntry> ();
		}
	}
}

