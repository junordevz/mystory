﻿using System;

namespace StoryModels
{
	[Serializable]
	public class MapEntry : TextEntry, IEntry
	{
		public LatLon Position { get; set; }

		public MapEntry () : base ()
		{
			Type = EntryType.Map;
			Position = new LatLon ();
		}
	}
}

