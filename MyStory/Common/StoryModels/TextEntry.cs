﻿using System;

namespace StoryModels
{
	[Serializable]
	public class TextEntry : IEntry
	{
		public long Id { get; set; }

		public DateTime TimeStamp { get; set; }

		public EntryType Type { get; set; }

		public string Title { get; set; }

		public string Content { get; set; }

		public TextEntry ()
		{
			Id = 0;
			TimeStamp = DateTime.Now;
			Type = EntryType.Text;
			Title = "";
			Content = "";
		}

		public long GetId()
		{
			return Id;
		}


		public void SetId(long id)
		{
			Id = id;
		}

		public DateTime GetTimeStamp()
		{
			return TimeStamp;
		}

		public string GetTitle()
		{
			return Title;
		}

		public string GetContent()
		{
			return Content;
		}

		public EntryType GetType()
		{
			return Type;
		}
	}
}

