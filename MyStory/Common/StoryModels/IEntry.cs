﻿using System;

namespace StoryModels
{
	public interface IEntry
	{
		long GetId();

		void SetId(long id);

		DateTime GetTimeStamp();

		string GetTitle();

		string GetContent();

		EntryType GetType();
	}

	public enum EntryType
	{
		Text,
		Map,
		Image,
		Video,
		Audio
	}
}

